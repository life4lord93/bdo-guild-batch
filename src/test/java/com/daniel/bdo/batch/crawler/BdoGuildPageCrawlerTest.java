package com.daniel.bdo.batch.crawler;

import java.time.Duration;

import org.junit.jupiter.api.Test;

import reactor.core.publisher.Mono;

public class BdoGuildPageCrawlerTest {

	@Test
	public void getGuildListTest() {
		BdoGuildPageCrawler bdoGuildPageCrawler = new BdoGuildPageCrawler();

		for (int i = 0; i < 930; i++) {
			Mono<String> mono = bdoGuildPageCrawler.getBdoGuildPage(i).delayElement(Duration.ofSeconds(1));
			mono.subscribe(html -> System.out.println(html.length()));
		}

	}
}
