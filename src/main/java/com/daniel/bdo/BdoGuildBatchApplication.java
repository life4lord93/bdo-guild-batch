package com.daniel.bdo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BdoGuildBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(BdoGuildBatchApplication.class, args);
	}

}
