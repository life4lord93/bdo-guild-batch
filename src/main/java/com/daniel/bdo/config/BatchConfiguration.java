package com.daniel.bdo.config;

import javax.sql.DataSource;

import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BatchConfiguration extends DefaultBatchConfigurer {
	@Override
	public void setDataSource(DataSource dataSource) {
	}
}
