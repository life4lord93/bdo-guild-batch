package com.daniel.bdo.model;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class BdoGuild {
	private String name;
	private String url;
	private List<BdoFamily> familyList;
}
