package com.daniel.bdo.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BdoCharacter {
	private String name;
	private Integer level;
}
