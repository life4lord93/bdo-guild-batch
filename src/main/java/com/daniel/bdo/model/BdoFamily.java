package com.daniel.bdo.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BdoFamily {
	private String name;
	private List<BdoCharacter> bdoCharacterList;
}
