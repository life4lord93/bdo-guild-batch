package com.daniel.bdo.batch.step.guild;

import org.springframework.batch.item.ItemWriter;

import com.daniel.bdo.model.BdoGuild;

public interface BdoGuildWriter extends ItemWriter<BdoGuild> {
}
