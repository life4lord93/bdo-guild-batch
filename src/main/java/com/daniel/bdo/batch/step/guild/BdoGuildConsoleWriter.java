package com.daniel.bdo.batch.step.guild;

import java.util.List;

import com.daniel.bdo.model.BdoGuild;

public class BdoGuildConsoleWriter implements BdoGuildWriter {

	@Override
	public void write(List<? extends BdoGuild> list) throws Exception {
		for (BdoGuild bdoGuild : list) {
			System.out.println(bdoGuild);
		}
	}
}
