package com.daniel.bdo.batch.step.guild;

import org.springframework.batch.item.ItemReader;

import com.daniel.bdo.model.BdoGuild;

public interface BdoGuildReader extends ItemReader<BdoGuild> {

}
