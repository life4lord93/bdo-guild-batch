package com.daniel.bdo.batch.step.guild;

import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.daniel.bdo.batch.crawler.BdoGuildPageCrawler;
import com.daniel.bdo.batch.parser.BdoGuildParser;
import com.daniel.bdo.model.BdoGuild;

public class BdoGuildCrawlerReader implements BdoGuildReader {

	private BdoGuildPageCrawler crawler;
	private BdoGuildParser parser;

	@Override
	public BdoGuild read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		return null;
	}
}
