package com.daniel.bdo.batch.crawler;

import org.springframework.web.reactive.function.client.WebClient;

import reactor.core.publisher.Mono;

public class BdoGuildPageCrawler {
	private static final String BDO_GUILD_SEARCH_URL = "https://www.kr.playblackdesert.com/Adventure/Guild?Page=%d";
	private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36";
	private WebClient webClient;

	public BdoGuildPageCrawler() {
		webClient = WebClient.builder().build();
	}

	public Mono<String> getBdoGuildPage(int page) {
		return webClient.get()
			.uri(String.format(BDO_GUILD_SEARCH_URL, page))
			.header("user-agent", USER_AGENT)
			.retrieve()
			.bodyToMono(String.class);
	}

	public Mono<String> getBdoGuildDefaultPage() {
		return this.getBdoGuildPage(1);
	}
}
