package com.daniel.bdo.batch.parser;

import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.daniel.bdo.model.BdoGuild;

public class BdoGuildParser {

	private static final String GUILD_LIST_SELECTOR = ".box_list_area .adventure_list_table";
	private static final String GUILD_TITLE_SELECTOR = ".guild_title .text a";
	private static final String GUILD_PAGE_NO_SELECTOR = ".paging a:not(.btn_arrow)";

	public List<BdoGuild> parseToBdoGuildList(String html) {
		List<BdoGuild> bdoGuildList = new ArrayList<>();
		Document doc = Jsoup.parse(html);

		Elements guildElements = doc.select(GUILD_LIST_SELECTOR).select("li");

		guildElements.forEach(element -> {
			Element guildElement = element.selectFirst(GUILD_TITLE_SELECTOR);
			String guildName = guildElement.text();
			String guildUrl = guildElement.attr("href");
			bdoGuildList.add(BdoGuild.builder().name(guildName).url(guildUrl).build());
		});

		return bdoGuildList;
	}

	public int parseToBdoGuildPageMaxNo(String html) {
		Document doc = Jsoup.parse(html);

		Element maxPageElement = doc.select(GUILD_PAGE_NO_SELECTOR).last();

		if (maxPageElement == null) {
			return 1;
		}

		return Integer.parseInt(maxPageElement.text());
	}
}
